function calculateAscendingValue() {
    const inputValue = document.getElementById("number").value;
    if (inputValue.length > 3 || inputValue.length < 3) {
        return;
    }
    const numberFirstly = Math.floor(inputValue / 100) * 1;
    const numberBetween = (Math.floor(inputValue / 10) % 10) * 1;
    const numberFinal = (inputValue % 10) * 1;
    console.log(numberFirstly, numberBetween, numberFinal);
    if (numberFirstly >= numberBetween && numberFirstly >= numberFinal) {
        if (numberBetween >= numberFinal) {
            document.getElementById("smallNumber").innerHTML = `${numberFinal}`;
            document.getElementById(
                "middleNumber"
            ).innerHTML = `${numberBetween}`;
            document.getElementById("bigNumber").innerHTML = `${numberFirstly}`;
        } else {
            document.getElementById(
                "smallNumber"
            ).innerHTML = `${numberBetween}`;
            document.getElementById(
                "middleNumber"
            ).innerHTML = `${numberFinal}`;
            document.getElementById("bigNumber").innerHTML = `${numberFirstly}`;
        }
    } else if (numberFirstly >= numberBetween && numberFirstly < numberFinal) {
        document.getElementById("smallNumber").innerHTML = `${numberBetween}`;
        document.getElementById("middleNumber").innerHTML = `${numberFirstly}`;
        document.getElementById("bigNumber").innerHTML = `${numberFinal}`;
    } else if (numberFirstly < numberBetween && numberFirstly >= numberFinal) {
        document.getElementById("smallNumber").innerHTML = `${numberFinal}`;
        document.getElementById("middleNumber").innerHTML = `${numberFirstly}`;
        document.getElementById("bigNumber").innerHTML = `${numberBetween}`;
    } else {
        if (numberBetween >= numberFinal) {
            document.getElementById("smallNumber"
            ).innerHTML = `${numberFirstly}`;
            document.getElementById(
                "middleNumber"
            ).innerHTML = `${numberFinal}`;
            document.getElementById("bigNumber").innerHTML = `${numberBetween}`;
        } else {
            document.getElementById(
                "smallNumber"
            ).innerHTML = `${numberFirstly}`;
            document.getElementById(
                "middleNumber"
            ).innerHTML = `${numberBetween}`;
            document.getElementById("bigNumber").innerHTML = `${numberFinal}`;
        }
    }
}

