function greeting() {
    const selectValue = document.getElementById("select").value;
    let message = "";
    switch (selectValue) {
        case "B": {
            message = "Xin chào Bố";
            break;
        }
        case "M": {
            message = "Xin chào Mẹ";
            break;
        }
        case "A": {
            message = "Xin chào Anh trai";
            break;
        }
        default: {
            message = "Xin chào Em gái";
        }
    }
    document.getElementById("greeting").innerHTML = message;
}