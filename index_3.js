function calculateOddAndEvenNumbers(inputValue) {
    let evenNumber = 0;
    let odd = 0;
    const numberFirstly = Math.floor(inputValue / 100) * 1;
    const numberBetween = (Math.floor(inputValue / 10) % 10) * 1;
    const numberFinal = (inputValue % 10) * 1;
    if (inputValue.length > 3 || inputValue.length < 3) {
        return "Số không hợp lệ, Chỉ nhập 3 chữ số nguyên dương !!! ";
    }
    if (numberFirstly % 2 === 0) {
        evenNumber = evenNumber + 1;
    } else {
        odd = odd + 1;
    }
    if (numberBetween % 2 === 0) {
        evenNumber = evenNumber + 1;
    } else {
        odd = odd + 1;
    }
    if (numberFinal % 2 === 0) {
        evenNumber = evenNumber + 1;
    } else {
        odd = odd + 1;
    }
    return `số lượng số chẵn là ${evenNumber} ----- Số lượng số lẻ là ${odd}`;
}
function integer() {
    const inputValue = document.getElementById("number2").value;
    const outputResults = calculateOddAndEvenNumbers(inputValue);
    document.getElementById("export").innerHTML = outputResults;
}
